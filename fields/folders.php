<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

JLoader::import('joomla.filesystem.folder');

JFormHelper::loadFieldClass('list');

class JFormFieldFolders extends JFormFieldList
{

    protected function getOptions()
    {
        $options = array();

        $filter = (string)$this->element['filter'];

        $path = (string)$this->element['directory'];
        if (!is_dir($path)) {
            $path = JPATH_ROOT . '/' . $path;
        }

        $folders = JFolder::folders($path, $filter, true, true);

        if (is_array($folders)) {
            foreach ($folders as $folder) {
                $folder = JString::str_ireplace(JPATH_ROOT . '/images/', '', $folder);
                $options[] = JHtml::_('select.option', $folder, $folder);
            }
        }

        $options = array_merge(parent::getOptions(), $options);

        return $options;
    }
}