<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

abstract class mod_mh_fading_images
{

    public static function firstLoad(stdClass $module, JRegistry &$params)
    {
        static $onload;

        if ($onload) {
            return;
        }

        $onload = true;

        JFactory::getDocument()->addScript(JUri::base(true) . '/modules/' . $module->module . '/js/sis.js');

        $css[] = 'div.sis_wrapper{position:relative;}';
        $css[] = 'div.sis_inner{';
        $css[] = 'position:absolute;';
        $css[] = 'overflow:hidden;';
        $css[] = 'z-index:2;';
        $css[] = 'display:none;';
        $css[] = 'width:100%;';
        $css[] = '}';

        JFactory::getDocument()->addStyleDeclaration(implode($css));
    }

    public static function getImagesFromFolder(JRegistry &$params)
    {
        JLoader::import('joomla.filesystem.folder');

        $images = JFolder::files(JPATH_ROOT . '/images/' . $params->get('folder'), '(.jpg|.JPG|.png|.PNG|.gif|.GIF)', false, true);

        if (empty($images)) {
            return null;
        }

        foreach ($images as &$image) {
            $image = JUri::root() . JString::str_ireplace(JPATH_ROOT, '', $image);
        }

        if ($params->get('shuffle')) {
            shuffle($images);
        }

        return $images;
    }
}