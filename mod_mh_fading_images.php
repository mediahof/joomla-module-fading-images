<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

$images = mod_mh_fading_images::getImagesFromFolder($params);

if (empty($images)) {
    return;
}

mod_mh_fading_images::firstLoad($module, $params);

require JModuleHelper::getLayoutPath($module->module, $params->get('layout', 'default'));