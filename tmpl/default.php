<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

$count = count($images);

$css[] = 'div.sis_' . $module->id . '.sis_wrapper{';
$css[] = 'width:' . ($params->get('width') ? $params->get('width') . 'px;' : '100%;');
$css[] = 'height:' . $params->get('height') . 'px;';
$css[] = '}';

if ($count == 1) {
    $css[] = 'div.sis_' . $module->id . ' .sis_inner{display:block;}';
}

JFactory::getDocument()->addStyleDeclaration(implode($css));
?>
<div class="sis_wrapper sis_<?php echo $module->id ?>">
    <?php foreach ($images as $key => &$image) : ?>
        <div class="sis_inner" id="<?php echo 'sis_' . $module->id . '_' . ($key + 1); ?>">
            <?php echo JHtml::image($image, ''); ?>
        </div>
    <?php endforeach; ?>
    <?php if ($count > 1) : ?>
        <script type="text/javascript">
            new sis(<?php echo $count; ?>, <?php echo $params->get('fadeSpeed'); ?>, <?php echo $params->get('fadeOutTime')*1000; ?>, '<?php echo 'sis_'.$module->id.'_'; ?>');
        </script>
    <?php endif; ?>
</div>